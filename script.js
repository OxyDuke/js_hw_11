function togglePassword(inputId) {
  const passwordInput = document.getElementById(inputId);
  const passwordIcon = passwordInput.nextElementSibling;

  if (passwordInput.type === "password") {
    passwordInput.type = "text";
    passwordIcon.className = "fas fa-eye-slash icon-password";
  } else {
    passwordInput.type = "password";
    passwordIcon.className = "fas fa-eye icon-password";
  }
}

function checkPassword() {
  const password = document.getElementById("password").value;
  const confirmPassword = document.getElementById("confirmPassword").value;

  if (password === confirmPassword) {
    alert("You are welcome");
    document.querySelector(".password-form").reset(); 
  } else {
    const errorMessage = document.createElement("div");
    errorMessage.style.color = "red";
    errorMessage.textContent = "Потрібно ввести однакові значення";
    document.querySelector(".password-form").appendChild(errorMessage);
    return false;
  }
}
